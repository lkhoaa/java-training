package lession03;

public class ArrayClass {
    public static void main(String[] args) {
        // Khoi tao mang
        int[] intArr = new int[5];

        // Gan phan tu cho mang da khoi tao
        intArr[0] = 5;
        intArr[1] = 4;
        intArr[2] = 3;
        intArr[3] = 2;
        intArr[4] = 1;


        // Lay phan tu tai vi tri bat ky
        int value = intArr[2];
        System.out.println("Index 2: " + value);

        // Thay doi gia tri:
        intArr[2] = 10;
        value = intArr[2];
        System.out.println("Index 2: " + value);


        // Gan phan tu
        int arrName[] = {1, 2, 3, 4, 5};
        value = arrName[4];
        System.out.println("Index 4: " + value);
        System.out.println("Kich thuoc mang : " + arrName.length);


        System.out.println("Print a array: ");
        for (int number : arrName) {
            System.out.print(number + " ");
        }

        // Tim so le
        int num = 3;
        isSoLe(3);

        num = 10;
        isSoLe(num);
        // Check so le

        for (int i = 0; i < 10; i++) {
            //            if (i % 2 != 0) {
            //                System.out.print(i + " ");
            //            }

            isSoLe(i);
        }

    }

    public static boolean isSoLe(int number) {
        if (number % 2 != 0) {
            System.out.println("So le " + number);
            return true;
        }
        return false;
    }

    public static void inputTextTo(String des, String value) {
        // Xu ly logic
    }


}
